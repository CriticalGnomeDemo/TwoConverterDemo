package com.criticalgnome.twoconverterdemo

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.jaxb.JaxbConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.lang.reflect.Type

class XmlOrJsonConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(type: Type?, annotations: Array<Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, *>? {
        annotations?.forEach { annotation ->
            when (annotation.annotationClass) {
                Xml::class.java -> return JaxbConverterFactory.create().responseBodyConverter(type, annotations, retrofit)
                Json::class.java -> return MoshiConverterFactory.create().responseBodyConverter(type, annotations, retrofit)
            }
        }
        return MoshiConverterFactory.create().responseBodyConverter(type, annotations, retrofit)
    }

    companion object {
        fun create() = XmlOrJsonConverterFactory()
    }
}
