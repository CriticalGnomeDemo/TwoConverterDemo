package com.criticalgnome.twoconverterdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.android.Main
import kotlinx.coroutines.experimental.launch

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        GlobalScope.launch(Dispatchers.Main) {
            val postsRequest = JsonPlaceholderApi.getApi().getPosts()
            val postsResponse = postsRequest.await()
            postsResponse.body()?.forEach { post -> Log.d("POSTS", post.title) }
        }
    }
}
